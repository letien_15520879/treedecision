import numpy as np
import pandas as pd
import math

def readData(path): # read data
    df = pd.DataFrame.from_csv(path)
    X = df.iloc[:, :-1] #data train
    y = df.iloc[:,-1]  # label train
    return X,y

def colum(data,atrr): #column data
    return data[atrr]

def featureColum(dataColumn):
    _list = dataColumn.unique()
    return  _list

def subData(data,listIndex):
    subData = data.iloc[listIndex, :]
    return  subData

def listIndex(data,att,feature): #get idex of feature
    return data.index[data[att] == feature].tolist()

def getLabel(target,listIndex): #tập taget với listIndex tuong ưng
    return  target[listIndex]

def calI (target): #calculate I
    a = target.tolist()
    labels = target.unique()
    value = 0
    for i in labels:
        x = a.count(i)/len(a)
        value += -x*(math.log(x,2))
    return  value

def calG(data,att,target): #calculate Gain
    col = colum(data,att)
    _featureColum = featureColum(col)
    G = calI(target)
    for i in _featureColum:
        list  = listIndex(data,att,i)
        label = getLabel(target, list)
        I = calI(label)
        N = data.count()[0]
        G = G - (len(list)/N)*I
    return G

def maxG(data,target): #max Gain
    attributes = list(data)
    listG = [calG(data,i,target) for i in attributes ] #tinh Gain cho moi thuoc tinh
    index = listG.index(max(listG)) # index has values max

    return attributes[index]

def train(_data,_target): # train data
    listFather = [] #list node father
    listChidren = [] #list node children
    flag = [["root"]] #flag node father _ children

    listData = [_data]
    listTarget = [_target]

    while(len(listData) > 0):
        data = listData[0]
        target = listTarget[0]
        att = maxG(data,target)
        ft = featureColum(data[att])
        for i in ft:
            _list = listIndex(data,att,i)
            list = [i-1 for i in _list]
            _lTarget = getLabel(target,_list).unique()
            if len(_lTarget) == 1:
                listFather.append(flag[0])
                listChidren.append([att,i])
                listFather.append([att,i])
                listChidren.append(_lTarget[0])
            else:
                flag.append([att,i])
                subdata = data.iloc[list]
                subtarget = target[_list]
                subdata = subdata.drop(att,axis=1)
                listData.append(subdata)
                listTarget.append(subtarget)
                listFather.append(flag[0])
                listChidren.append([att,i])

        del listData[0]  #update data
        del listTarget[0] #update label
        del flag[0]  #flag children of father
    return listFather,listChidren


def predict(inputL,data,target):
    tr = train(data,target)
    print("\n-----------------result Train---------------")
    resultTrain(tr,target)
    print("------------------result Predict------------\n")
    labels = target.unique().tolist() #list label
    attr = list(data)
    p = ['root']
    i = 0
    while i < len(tr[0]):
        for i in range(0, len(tr[0])):
            if p == tr[0][i]: #node father
                if tr[1][i] in labels: #check node leaf
                    return tr[1][i] #lable result
                elif tr[1][i][1] == inputL[attr.index(tr[1][i][0])]: #check node children == input(attr)
                    p = tr[1][i]
        i +=1
    return "not predict"

def resultTrain(train,target):
    labels = target.unique().tolist()
    result = []
    for i in range(0,len(train[1])):
        rule = []
        if train[1][i] in labels:
            rule.append(train[1][i])
            p = train[0][i]
            rule.append(p)
            while(p != ['root']):
                p = train[0][train[1].index(p)]
                rule.append(p)
            result.append(rule)
    for list in result:
        list.reverse()
        del list[0]
        s = ""
        for i in range(0,len(list)-1):
            if i >0: s +=" ^ "
            s +="{} : {}".format(list[i][0],list[i][1])
        s += " ==> {}".format(list[-1])
        print("{}\n".format(s))



if __name__ == "__main__":
     #path = 'D:/LE TIEN/HOC KY 7/ML NC/THUC HANH/Data/data1.csv'
     #path = 'D:/LE TIEN/HOC KY 7/ML NC/THUC HANH/Data/data2.csv'
     path = 'D:/LE TIEN/HOC KY 7/ML NC/THUC HANH/Data/data3.csv'

     data = readData(path)
     InputPredict = ['Female','2','Expensive','High']
     print(predict(InputPredict,data[0],data[1]))

